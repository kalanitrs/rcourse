install.packages('ggplot2')
library(ggplot2)
install.packages('caTools')
library(caTools)

setwd("~/������ ������/��� �'/����� �'/����� �����")
houses.raw <- read.csv('kc_house_data.csv')
str(houses.raw)
summary(houses.raw)

houses.prepared <- houses.raw
houses.prepared$id <- NULL
houses.prepared$zipcode <- NULL

any(is.na(houses.prepared)) #no missing values

#date - only year and month are needed
Sys.setlocale("LC_TIME", "English")
houses.prepared$date <- as.character(houses.prepared$date)
houses.prepared$year <- substr(houses.prepared$date,1,4)
houses.prepared$year <- as.integer(houses.prepared$year)
houses.prepared$month <- substr(houses.prepared$date,5,6)
houses.prepared$month <- as.factor(houses.prepared$month)
houses.prepared$date <- NULL

#checking if month+year affects
houses.prepared$ym <- paste(houses.prepared$year,houses.prepared$month,sep="")
houses.prepared$ym <- as.integer(houses.prepared$ym)

ggplot(houses.prepared,aes(as.factor(ym),price)) + geom_boxplot() #looks like not
houses.prepared$ym <- NULL

#checking if renovated affects price
set_renovate <- function(x) {
  if (x==0) return (FALSE)
  return (TRUE)
}

houses.prepared$renovate <- sapply(houses.prepared$yr_renovated,set_renovate)
ggplot(houses.prepared,aes(renovate,price)) + geom_boxplot() #looks like it is
houses.prepared$renovate <- NULL

eras <- c(0,1934,1954,1974,1994,2016)
labels <- c("not renovated", "very old renov", "old renov", "renovated", "recently renov")
bins <- cut(houses.prepared$yr_renovated,breaks = eras,labels = labels,include.lowest = T,right = F)
houses.prepared$yr_renovated <- bins
ggplot(houses.prepared, aes(yr_renovated, price)) + geom_boxplot()

#split into train and test
filter <- sample.split(houses.prepared$bedrooms,SplitRatio = 0.7)
houses.train <- subset(houses.prepared,filter == TRUE)
houses.test <- subset(houses.prepared,filter == FALSE)

model <- lm(price~., houses.train)
summary(model)

predicted.test <- predict(model,houses.test)
MSE.test <- mean((houses.test$price-predicted.test)**2)
RMSE <- MSE.test**0.5

predicted.train <- predict(model,houses.train)
MSE.train <- mean((houses.train$price-predicted.train)**2)
RMSE.train <- MSE.train**0.5
